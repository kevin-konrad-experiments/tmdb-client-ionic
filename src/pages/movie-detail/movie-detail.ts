import { FavoriteMovieProvider } from "../../providers/favorite-movie/favorite-movie";
import { IMovie } from "../../interface/IMovie";
import { Component } from "@angular/core";
import { IonicPage, NavParams } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-movie-detail",
  templateUrl: "movie-detail.html"
})
export class MovieDetailPage {
  private movie: IMovie;
  private isFavorite: boolean = false;

  constructor(private navParams: NavParams, private favoriteMovieProvider: FavoriteMovieProvider) {}

  ionViewDidLoad() {
    this.movie = this.navParams.data;
    this.isFavorite = this.favoriteMovieProvider.isFavoriteMovie(this.movie);
  }

  toggleFavorite(): void {
    this.isFavorite = !this.isFavorite;
    this.favoriteMovieProvider.toggleFavoriteMovie(this.movie);
  }
}
