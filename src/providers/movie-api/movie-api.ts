import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Platform } from "ionic-angular";
import { Observable } from "rxjs/Rx";
import { IMovie } from "../../interface/IMovie";

@Injectable()
export class MovieApiProvider {
  private readonly baseUrl: string = "../../assets/api/movies.json";

  constructor(private http: HttpClient, private platform: Platform) {
    if (this.platform.is("cordova") && this.platform.is("android")) {
      this.baseUrl = "/android_asset/www/assets/api/movies.json";
    }
  }

  getMovies(): Observable<IMovie[]> {
    return this.http.get<IMovie[]>(`${this.baseUrl}`);
  }
}
